FROM maven:3-eclipse-temurin-21 as build
WORKDIR /app
COPY . .
RUN mvn clean package

FROM eclipse-temurin:21-jre-jammy
WORKDIR /app
COPY --from=build /app/target/Jetbrains-Help.jar Jetbrains-Help.jar
ENV TZ=Asia/Shanghai
RUN ln -sf /usr/share/zoneinfo/{TZ} /etc/localtime && echo "{TZ}" > /etc/timezone
EXPOSE 10768
ENTRYPOINT ["java", "-jar", "Jetbrains-Help.jar"]